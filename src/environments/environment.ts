// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyCk35mjHdMTg7ExtD0BhtUAFezhBqG8Dh8',
    authDomain: 'conf-e-cine.firebaseapp.com',
    projectId: 'conf-e-cine',
    storageBucket: 'conf-e-cine.appspot.com',
    messagingSenderId: '835289492076',
    appId: '1:835289492076:web:e9a24dde5cf06c64ba4649',
  },
  tmdb:'804f1ecfc9dc9b86567968a986da7588'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
