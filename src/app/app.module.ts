import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FeaturesModule } from './features/features.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './features/menu/menu/menu.component';
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    FeaturesModule,
    RouterModule.forRoot([]),
    AngularFireModule.initializeApp(environment.firebaseConfig, 'Conf-e-cine'),
    AngularFireAuthModule,
    AngularFirestoreModule,
    NoopAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
