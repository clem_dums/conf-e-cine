import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { FilmInfo, FilmList, Credits } from "../models/filmTmdb";


export interface SearchMoviesFilters{
    with_genres:string;
    year:string;
    language:string;
    with_runtime:{
        gte:string;
        lte:string;
    }
}
@Injectable({
    providedIn: 'root',
  })
export class FilmWebService{

    private readonly baseUrl: string;
    apiKey=environment.tmdb

    constructor(private readonly httpClient: HttpClient) {
      this.baseUrl = 'https://api.themoviedb.org/3';
    }

    searchMovies(query:string,language?:string,year?:string):Observable<FilmList>{
        return this.httpClient.get<FilmList>(`${this.baseUrl}/search/movie?api_key=${this.apiKey}&query=${query}&language=${language??'fr-FR'}&year=${year??''}`);
    }

    getMovies(options?:SearchMoviesFilters):Observable<FilmList>{
        return this.httpClient.get<FilmList>(`${this.baseUrl}/discover/movie?api_key=${this.apiKey}&language=${options?.language??'fr-FR'}&primary_release_year=${options?.year??''}&with_genres=${options?.with_genres??''}&with_runtime.lte=${options?.with_runtime?.lte??''}`);
    }

    getMovieInfo(movieId: number):Observable<FilmInfo>{
        return this.httpClient.get<FilmInfo>(`${this.baseUrl}/movie/${movieId}?api_key=${this.apiKey}&language=fr-FR`);
    }

    getCredits(id:number):Observable<Credits>{
        return this.httpClient.get<Credits>(`${this.baseUrl}/movie/${id}/credits?api_key=${this.apiKey}`);
    }
}