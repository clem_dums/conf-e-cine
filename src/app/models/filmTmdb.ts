export interface FilmList {
  page: number;
  results: FilmResult[];
  total_results: number;
  total_pages: number;
}

export interface FilmResult {
  poster_path?: string, 
  adult: boolean,
  overview: string,
  release_date: string,
  genre_ids: number[],
  id: number,
  original_title: string,
  original_language: string,
  title: string,
  backdrop_path?: string,
  popularity: number,
  vote_count: number,
  video: boolean,
  vote_average: number 
}

export interface Credits {
  id: number;
  cast: Person[];
}

export interface Person {
  name: string;
  profile_path?: string;
}

export interface FilmInfo{
  adult: boolean,
  backdrop_path?: string,
  belongs_to_collection: any,
  budget: number,
  genres: genre[],
  homepage: string,
  id:number,
  imbd_id:string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number,
  poster_path?: string, 
  production_companies: production_companies[],
  production_countries: production_countries[],
  release_date: string,
  revenue: number,
  runtimes: number,
  spoken_language: spoken_language[],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number
}

export interface genre{
  id:number, 
  name: string
}

export interface production_companies{
  name: string,
  id: number,
  logo_path: string,
  origin_contry: string
}

export interface production_countries{
  iso_3166_1: string,
  name: string
}

export interface spoken_language{
  iso_639_1: string,
  name: string
}