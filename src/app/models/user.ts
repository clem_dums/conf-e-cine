export interface User {
  id: string;
  firstName: string;
  lastName: string;
  displayName: string;
  email?: string;
  phoneNumber?: string;
  creationDate: string | Date;
  lastSignInDate: string | Date;
  photoUrl: string;
  filmsList: UserList[];
}

export interface UserList{
  id:string;
  films:string[];
  name:string;
}