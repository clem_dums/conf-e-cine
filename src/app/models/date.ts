export interface DateInput {
    day: number;
    month: number;
    year: number;
  }

export function toDateInput(date: Date | string): DateInput {
    const d = date ? new Date(date) : null;
    return d !== null
    ? {
        day: d.getDate(),
        month: d.getMonth() + 1,
        year: d.getFullYear(),
    }
    : null;
}
