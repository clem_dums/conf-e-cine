import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { FeaturesRoutingEnum } from '../features-routing-enum';
import { SignInServiceService } from '../services/sign-in/sign-in-service.service';

@Injectable()
export class MustBeConnectedGuard implements CanActivate {

  constructor(
    private readonly signInServiceService: SignInServiceService,
    private readonly router: Router,
  ) {}

  canActivate(): Observable<boolean | UrlTree>  {
    return this.signInServiceService.user$.pipe(
      map((user: User) => {
        if (!!user){
          return true;
        } else {
          this.router.navigate([FeaturesRoutingEnum.SignIn]);
          return false;
        }})
    );
  }
}
