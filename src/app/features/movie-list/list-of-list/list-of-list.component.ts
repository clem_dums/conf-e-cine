import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-of-list',
  templateUrl: './list-of-list.component.html',
  styleUrls: ['./list-of-list.component.scss']
})
export class ListOfListComponent implements OnInit {

  @Input() listName: string;
  @Input() listId: number;

  constructor() { 
  }

  ngOnInit(): void {
  }

}
