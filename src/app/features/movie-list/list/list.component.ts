import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { CreateListDialogComponent } from '../create-list-dialog/create-list-dialog.component';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';
import { User, UserList } from 'src/app/models/user';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, first, map, switchMap, take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  get user$(): Observable<User>{
    return this.signInService.user$  
  }

  userLists$ : Observable<UserList[]>;

  constructor(
    private readonly location: Location,
    private dialog: MatDialog,
    private signInService: SignInServiceService,
    private readonly angularFirestore: AngularFirestore
  ) {
    this.userLists$ = this.user$.pipe(distinctUntilChanged(),map((user:User)=>user.filmsList))
  }

  ngOnInit(): void {
  }

  goBack(){
    this.location.back();
  }

  createList(userId:string){

    const dialogRef = this.dialog.open(CreateListDialogComponent, {
      width: '70%',
      data: {name:null}
    });

    dialogRef.afterClosed().pipe(
      first(),
      filter((result: string)=> !!result),
      switchMap((result: string) => 
        combineLatest([
          of(result),
          this.signInService.user$.pipe(first()),
        ])
      ),
      switchMap(([result,user]:[string,User])=>
        combineLatest([
          of(result),
          this.angularFirestore.collection('users').doc(user.email).valueChanges().pipe(first())
        ])
      )
    ).subscribe(([name,user]:[string,User]) => {
        const id=`${user.filmsList.length}`;
        user.filmsList.push({id,name,films:[]})
        const filmsList= user.filmsList;
        this.angularFirestore.collection('users').doc(user.email).update({...user,filmsList})
    })
  }
}
