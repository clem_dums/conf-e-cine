import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface CreateListDialogData {
  name: string;
}

@Component({
  selector: 'app-create-list-dialog',
  templateUrl: './create-list-dialog.component.html',
  styleUrls: ['./create-list-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateListDialogComponent {
  createListFormGroup: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CreateListDialogData) {

      this.createListFormGroup = this.formBuilder.group({
        name: ['', Validators.required],
      });

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirm():void {
    this.dialogRef.close(this.createListFormGroup.get('name').value)
  }

}
