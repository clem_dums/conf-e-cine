import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ListOfListComponent } from './list-of-list/list-of-list.component';
import { PreviewListComponent } from './preview-list/preview-list.component';
import {MatDialogModule} from '@angular/material/dialog';
import { CreateListDialogComponent } from './create-list-dialog/create-list-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { FilmPreviewListComponent } from './preview-list/film-preview-list/film-preview-list.component';
import { LanguagePipeModule } from 'src/app/pipes/language-pipe';
import { ConfirmDeleteListComponent } from './preview-list/confirm-delete-list/confirm-delete-list.component';
import { MatButtonModule } from '@angular/material/button';
import { ConfirmDeleteMovieComponent } from './preview-list/film-preview-list/confirm-delete-movie/confirm-delete-movie.component';
@NgModule({
  imports: [
    CommonModule,
     RouterModule.forChild([
      {
        path: '',
        component: ListComponent,
      },
      {
        path:':id',
        component:PreviewListComponent
      }
    ]),
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    LanguagePipeModule,
    MatButtonModule
  ],
  providers: [],
  declarations: [
    ListComponent,
    ListOfListComponent,
    PreviewListComponent,
    CreateListDialogComponent,
    FilmPreviewListComponent,
    ConfirmDeleteListComponent,
    ConfirmDeleteMovieComponent,
   ],
})
export class MovieListModule {}
