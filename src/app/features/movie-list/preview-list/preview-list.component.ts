import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { User, UserList } from 'src/app/models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { filter,first, switchMap } from 'rxjs/operators';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';
import { FeaturesRoutingEnum } from '../../features-routing-enum';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDeleteListComponent} from './confirm-delete-list/confirm-delete-list.component';

@Component({
  selector: 'app-preview-list',
  templateUrl: './preview-list.component.html',
  styleUrls: ['./preview-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewListComponent implements OnInit {

  get user$():Observable<User>{
    return this.signInServiceService.user$;
  }

  id=this.router.url.split('/')[2]

  get userList$(): Observable<UserList[]>{
    return this.signInServiceService.userList$;
  }

  constructor(
    private readonly location: Location,
    private readonly router:Router,
    private readonly signInServiceService:SignInServiceService,
    private readonly angularFirestore:AngularFirestore,
    private dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.id=this.router.url.split('/')[2]
  }

  goBack(): void {
    this.location.back();
  }

  deleteList(userEmail:string, listName:string){
    const listId=this.router.url.split('/')[2]
    const dialogRef = this.dialog.open(ConfirmDeleteListComponent, {
      width: '70%',
      data: {listName}
    });

    dialogRef.afterClosed().pipe(
      first(),
      filter((result: boolean)=> !!result),
      switchMap((_:boolean)=>
        this.angularFirestore.collection('users').doc(userEmail).valueChanges().pipe(first())
      )
    ).subscribe((user:User) => {
      user.filmsList.filter((filmList:UserList)=> filmList.id !== listId)
      const filmsList = user.filmsList.filter((filmList:UserList)=> filmList.id !== listId)
      filmsList.map((films:UserList,index:number)=>{
        films.id=`${index}`;
      });
      this.angularFirestore.collection('users').doc(user.email).update({...user,filmsList})
      this.router.navigate([FeaturesRoutingEnum.List])
    })
  }

}