import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete-list',
  templateUrl: './confirm-delete-list.component.html',
  styleUrls: ['./confirm-delete-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmDeleteListComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDeleteListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {listName:string}) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirm():void {
    this.dialogRef.close(true)
  }
}
