import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmPreviewListComponent } from './film-preview-list.component';

describe('FilmPreviewListComponent', () => {
  let component: FilmPreviewListComponent;
  let fixture: ComponentFixture<FilmPreviewListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmPreviewListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmPreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
