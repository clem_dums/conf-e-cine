import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';
import { FeaturesRoutingEnum } from 'src/app/features/features-routing-enum';
import { SignInServiceService } from 'src/app/features/services/sign-in/sign-in-service.service';
import { FilmInfo } from 'src/app/models/filmTmdb';
import { User, UserList } from 'src/app/models/user';
import { FilmWebService } from 'src/app/webservices/listFilm';
import { ConfirmDeleteListComponent } from '../confirm-delete-list/confirm-delete-list.component';
import { ConfirmDeleteMovieComponent } from './confirm-delete-movie/confirm-delete-movie.component';

@Component({
  selector: 'app-film-preview-list',
  templateUrl: './film-preview-list.component.html',
  styleUrls: ['./film-preview-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilmPreviewListComponent implements OnInit {
  @Input() filmId:string;
  @Input() index:number;
  @Input() listName:string;

  get user$():Observable<User>{
    return this.signInServiceService.user$;
  }

  filmInfo$:Observable<FilmInfo>;

  constructor(
    private filmWebService:FilmWebService,
    private router:Router,
    private signInServiceService:SignInServiceService,
    private angularFirestore: AngularFirestore,
    private dialog: MatDialog
    ) {
  }

  ngOnInit(): void {
    this.filmInfo$ = this.filmWebService.getMovieInfo(+this.filmId);
  }

  SupprimerFilmListe(email:string,filmName:string){
    const listId=this.router.url.split('/')[2]
    const dialogRef = this.dialog.open(ConfirmDeleteMovieComponent, {
      width: '70%',
      data: {listName:this.listName, filmName}
    });

    dialogRef.afterClosed().pipe(
      first(),
      filter((result: boolean)=> !!result),
      switchMap((_:boolean)=>
        this.angularFirestore.collection('users').doc(email).valueChanges().pipe(first())
      )
    ).subscribe((user:User) =>
      {
        user.filmsList.find((filmList:UserList)=> filmList.id === listId).films.splice(this.index,1)
        const filmsList= user.filmsList;
        this.angularFirestore.collection('users').doc(user.email).update({...user,filmsList})
      });
  }

}
