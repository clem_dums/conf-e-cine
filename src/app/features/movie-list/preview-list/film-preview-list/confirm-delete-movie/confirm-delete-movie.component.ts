import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete-movie',
  templateUrl: './confirm-delete-movie.component.html',
  styleUrls: ['./confirm-delete-movie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmDeleteMovieComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDeleteMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {filmName:string, listName:string}) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirm():void {
    this.dialogRef.close(true)
  }

}
