import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { User, UserList } from 'src/app/models/user';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';

@Component({
  selector: 'app-add-list-dialog',
  templateUrl: './add-list-dialog.component.html',
  styleUrls: ['./add-list-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddListDialogComponent {

  addToListFormGroup: FormGroup;

  get user$():Observable<User>{
    return this.signInServiceService.user$;
  }

  get userList$():Observable<UserList[]>{
    return this.signInServiceService.userList$;
  }

  get id():AbstractControl{
    return this.addToListFormGroup.get('id');
  }

  get createName():AbstractControl{
    return this.addToListFormGroup.get('createName');
  }

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly signInServiceService:SignInServiceService,
    public dialogRef: MatDialogRef<AddListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {id:string,createName:string,connected:boolean}) {

      this.addToListFormGroup = this.formBuilder.group({
        id: ['', Validators.required],
        createName: ['', Validators.required],
      });

    }
  
    stopPropagation(event){
      event.stopPropagation();
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirm():void {
    this.dialogRef.close({
      id: this.id.value,
      createName: this.createName.value,
      connected:true
    });
  }

  goToLoginPage():void{
    this.dialogRef.close({
      id: null,
      createName: null,
      connected: false
    })
  }
}