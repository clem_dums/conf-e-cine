import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Person } from 'src/app/models/filmTmdb';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActorComponent implements OnInit {

  @Input() actor: Person;

  ngOnInit() {
    if (this.actor.profile_path == null){
      this.actor.profile_path = "https://www.searchpng.com/wp-content/uploads/2019/02/Profile-ICon.png";
    }
    
    else
    {
      let lien = "https://www.themoviedb.org/t/p/w260_and_h390_bestv2/";
      this.actor.profile_path = lien.concat(this.actor.profile_path.toString());
    }
  }
}