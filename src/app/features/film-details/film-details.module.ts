import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { RouterModule } from "@angular/router";
import { FilmDetailsComponent } from "./film-details/film-details.component";
import { MatMenuModule } from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSliderModule} from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import { ActorComponent } from "./actor/actor.component";
import { AddListDialogComponent } from './add-list-dialog/add-list-dialog.component';
import { MatDialogModule } from "@angular/material/dialog";
import { LanguagePipeModule } from "src/app/pipes/language-pipe";

@NgModule({
    imports: [
      CommonModule,
       RouterModule.forChild([
        {
          path: '',
          component: FilmDetailsComponent,
        }
      ]),
      ReactiveFormsModule,
      MatFormFieldModule,
      MatInputModule,
      MatMenuModule,
      MatIconModule,
      MatSliderModule,
      MatSelectModule,
      MatButtonModule,
      MatDialogModule,
      LanguagePipeModule
    ],
    providers: [],
    declarations: [
      FilmDetailsComponent,
      ActorComponent,
      AddListDialogComponent
    ],
  })
  export class FilmDetailsModule {}
  