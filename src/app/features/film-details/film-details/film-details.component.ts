import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Credits, FilmInfo, FilmResult } from 'src/app/models/filmTmdb';
import { filter, first, switchMap, take, tap } from 'rxjs/operators';
import { FilmWebService } from 'src/app/webservices/listFilm';
import { Location } from '@angular/common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AddListDialogComponent } from '../add-list-dialog/add-list-dialog.component';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';
import { User, UserList } from 'src/app/models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { FeaturesRoutingEnum } from '../../features-routing-enum';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilmDetailsComponent implements OnInit {

  film$: Observable<FilmInfo>;
  credits$: Observable<Credits>;

  get userLists$():Observable<UserList[]>{
    return this.signInService.userList$;
  }

  idFilm:string;

  private   languages:{iso_639_1:string, french_name:string, name:string, }[] = [
    {
      iso_639_1: '',
      french_name: "Tous",
      name: ''
    }, 
    {
      iso_639_1: "de",
      french_name: "Allemand",
      name: "Deutsch"
    },
    
    {
      iso_639_1: "fr",
      french_name: "Français",
      "name": "Français"
    },
    {
      iso_639_1: "it",
      french_name: "Italien",
      name: "Italiano"
    },
    {
      iso_639_1: "ru",
      french_name: "Russe",
      name: "Pусский"
    },
    {
      iso_639_1: "es",
      french_name: "Espagnol",
      name: "Español"
    },
    {
      iso_639_1: "en",
      french_name: "Anglais",
      name: "English"
    },
    ]


  constructor(
    private filmWebService:FilmWebService, 
    private route: ActivatedRoute,
    private readonly location: Location,
    private dialog: MatDialog,
    private signInService:SignInServiceService,
    private readonly angularFirestore:AngularFirestore,
    private router: Router
    ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idFilm = params['id'];
    });
    this.film$ = this.filmWebService.getMovieInfo(+this.idFilm);

    this.credits$ = this.filmWebService.getCredits(+this.idFilm);
  }

  goBack(): void {
    this.location.back();
  }

  addList(): void {
      const dialogRef = this.dialog.open(AddListDialogComponent, {
        width: '70%',
        data: {name:null}
      });

      dialogRef.afterClosed().pipe(
        first(),
        filter((result:{id:string,createName:string,connected:boolean})=> !!result),
        filter((result:{id:string,createName:string,connected:boolean})=> !!result.connected),
        switchMap((result:{id:string,createName:string,connected:boolean}) => 
          combineLatest([
            of(result),
            this.signInService.user$.pipe(first()),
          ])
        ),
        switchMap(([result,user]:[{id:string,createName:string,connected:boolean},User])=>
          combineLatest([
            of(result),
            this.angularFirestore.collection('users').doc(user.email).valueChanges().pipe(first())
          ])
        )
      ).subscribe(([result,user]:[{id:string,createName:string,connected:boolean},User]) => {
            const idFilm=this.router.url.split('?id=')[1];
            if(result.id==='~!//CreateList//!~'){
              const id=`${user.filmsList.length}`;
              const name=result.createName;
              user.filmsList.push({id,name,films:[idFilm]})
            }
            else {
              user.filmsList.find((filmList:UserList)=> filmList.id === result.id).films.push(idFilm);
            }
            const filmsList= user.filmsList;
            this.angularFirestore.collection('users').doc(user.email).update({...user,filmsList})
      })

      dialogRef.afterClosed().pipe(
        first(),
        filter((result:{id:string,createName:string,connected:boolean})=> !!result),
        filter((result:{id:string,createName:string,connected:boolean})=> !result.connected),
      ).subscribe((result:{id:string,createName:string,connected:boolean}) => {
          this.router.navigate([FeaturesRoutingEnum.SignIn]);
      })
  }
}
