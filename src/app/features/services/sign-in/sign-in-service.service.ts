import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { filter, distinctUntilChanged, map, takeUntil, first, take, switchMap } from 'rxjs/operators';
import { User, UserList } from 'src/app/models/user';
import { FeaturesRoutingEnum } from '../../features-routing-enum';

@Injectable({
  providedIn: 'root'
})
export class SignInServiceService {

  private readonly destroyed$ = new Subject<void>();
  isConnected$: Observable<boolean>;
  user$: Observable<User>;
  userList$:Observable<UserList[]>;

  constructor(
    private readonly angularFireAuth: AngularFireAuth,
    private readonly angularFirestore: AngularFirestore,
    private readonly router: Router
  ) {
    this.user$ = this.angularFireAuth.user.pipe(
      distinctUntilChanged(),
      switchMap((user:firebase.User) => combineLatest([of(user),this.angularFirestore.collection('users').doc(user?.email).valueChanges()])),
      map(([user,member]:[firebase.User,User]) => this.transformToUser(user,member?.filmsList)),
    ); 
    this.isConnected$ = this.user$.pipe(distinctUntilChanged(), map((user:User)=> !!user));
    this.userList$ = this.user$.pipe(distinctUntilChanged(), map((user:User) => user.filmsList));
    }

  transformToUser(user: firebase.User, filmsList: UserList[]): User {
    return !!user ? {
      id: user.uid,
      firstName: user.displayName.split(' ')[0],
      lastName: user.displayName.split(' ')[1],
      displayName: user.displayName,
      email: user.email,
      phoneNumber: user.phoneNumber,
      creationDate: user.metadata.creationTime,
      lastSignInDate: user.metadata.lastSignInTime,
      photoUrl: user.photoURL,
      filmsList:filmsList??[]
    } : null;
  }

  loginWithGoogle(): void {
    this.angularFireAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then(()=>{
      this.submitUserLogin();
    })
    .catch((error) => {
      if (error.code === 'auth/account-exists-with-different-credential') {
        const email = error.email;
        this.angularFireAuth.fetchSignInMethodsForEmail(email).then((methods) => {
          let provider = this.getProviderForProviderId(methods[0]);
          this.angularFireAuth.signInWithRedirect(provider).then(()=>{
            this.submitUserLogin();
          })
        });
      }
    })
  }

  loginWithFacebook(): void {
    this.angularFireAuth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then(()=>{
      this.submitUserLogin();
    })
    .catch((error) => {
      if (error.code === 'auth/account-exists-with-different-credential') {
        const email = error.email;
        this.angularFireAuth.fetchSignInMethodsForEmail(email).then((methods) => {
          let provider = this.getProviderForProviderId(methods[0]);
          this.angularFireAuth.signInWithRedirect(provider).then(()=>{
            this.submitUserLogin();
          })
        });
      }
    })
  }
  
  loginWithGithub(): void {
    this.angularFireAuth.signInWithPopup(new firebase.auth.GithubAuthProvider())
    .then(()=>{
      this.submitUserLogin();
    })
    .catch((error) => {
      if (error.code === 'auth/account-exists-with-different-credential') {
        const email = error.email;
        this.angularFireAuth.fetchSignInMethodsForEmail(email).then((methods) => {
          let provider = this.getProviderForProviderId(methods[0]);
          this.angularFireAuth.signInWithRedirect(provider).then(()=>{
            this.submitUserLogin();
          })
        });
      }
    })
  }
  
  getProviderForProviderId(method:string){
    if(method==='facebook.com'){
      console.log('facebook')
      return new firebase.auth.FacebookAuthProvider()
    }

    if(method==='google.com'){
      console.log('google')
      return new firebase.auth.GoogleAuthProvider()
    }

    if(method==='github.com'){
      console.log('try with git')
      return new firebase.auth.GithubAuthProvider();
    }

    console.log('Arlerte !!! - ',method)
  }

  submitUserLogin(): void{
    this.angularFireAuth.user.pipe(
      first(),
      filter((user: firebase.User) => !!user),
      switchMap((user:firebase.User) => combineLatest([of(user),this.angularFirestore.collection('users').doc(user.email).valueChanges().pipe(first())])),
    ).subscribe(([user,member]:[firebase.User,User]) =>
      {
        const newUser = this.transformToUser(user,member?.filmsList);
        this.angularFirestore
                .collection('users').doc(newUser.email)
                .set(newUser);
        this.router.navigate([FeaturesRoutingEnum.Member]);
      });
  }
  
  signOutUser(): void {
    this.angularFireAuth.signOut();
  }

  removeFilmFromList(userEmail:string,listId:string, index:number){
    this.angularFirestore.collection('users').doc(userEmail).valueChanges().pipe(
      first(),
    ).subscribe((user:User) =>
      {
        user.filmsList.find((filmList:UserList)=> filmList.id === listId).films.splice(index,1)
        const filmsList= user.filmsList;
        this.angularFirestore.collection('users').doc(user.email).update({...user,filmsList})
      });
  }

  deleteList(userEmail:string,listId:string){
    
  }

  onLeavePage(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

}
