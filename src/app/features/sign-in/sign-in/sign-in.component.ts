import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { Select } from '@ngxs/store';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent implements OnInit, OnDestroy {

  get isConnected$(): Observable<boolean>{
    return this.signInServiceService.isConnected$;
  }

  constructor(
    private readonly location: Location,
    private readonly signInServiceService: SignInServiceService,
  ) { }

  ngOnInit(): void {
  }

  loginWithFacebook(): void {
    this.signInServiceService.loginWithFacebook();
  }

  loginWithGithub(): void {
    this.signInServiceService.loginWithGithub();
  }

  loginWithGoogle(): void {
    this.signInServiceService.loginWithGoogle();
  }

  signOutUser(): void {
    this.signInServiceService.signOutUser();
  }

  goBack(): void {
    this.location.back();
  }

  ngOnDestroy(): void{
    this.signInServiceService.onLeavePage();
  }
}
