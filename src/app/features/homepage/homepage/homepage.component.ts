import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, first, map, switchMap, tap } from 'rxjs/operators';
import { FilmList, FilmResult } from 'src/app/models/filmTmdb';
import { FilmWebService, SearchMoviesFilters } from 'src/app/webservices/listFilm';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomepageComponent implements OnInit {

  years : number[]=
  [2000,2001,2002,null]

  listFilm$:Observable<FilmList>;
  genres: {id:number, name:string}[]=[
    {
      id: null,
      name: "Tous"
    },
    {
      id: 28,
      name: "Action"
    },
    {
      id: 12,
      name: "Adventure"
    },
    {
      id: 16,
      name: "Animation"
    },
    {
      id: 35,
      name: "Comedy"
    },
    {
      id: 80,
      name: "Crime"
    },
    {
      id: 99,
      name: "Documentary"
    },
    {
      id: 18,
      name: "Drama"
    },
    {
      id: 10751,
      name: "Family"
    },
    {
      id: 14,
      name: "Fantasy"
    },
    {
      id: 36,
      name: "History"
    },
    {
      id: 27,
      name: "Horror"
    },
    {
      id: 10402,
      name: "Music"
    },
    {
      id: 9648,
      name: "Mystery"
    },
    {
      id: 10749,
      name: "Romance"
    },
    {
      id: 878,
      name: "Science Fiction"
    },
    {
      id: 10770,
      name: "TV Movie"
    },
    {
      id: 53,
      name: "Thriller"
    },
    {
      id: 10752,
      name: "War"
    },
    {
      id: 37,
      name: "Western"
    },
  ]
      
  languages:{iso_639_1:string, french_name:string, name:string, }[] = [
  {
    iso_639_1: '',
    french_name: "Tous",
    name: ''
  }, 
  {
    iso_639_1: "de",
    french_name: "Allemand",
    name: "Deutsch"
  },
  
  {
    iso_639_1: "fr",
    french_name: "Français",
    "name": "Français"
  },
  {
    iso_639_1: "it",
    french_name: "Italien",
    name: "Italiano"
  },
  {
    iso_639_1: "ru",
    french_name: "Russe",
    name: "Pусский"
  },
  {
    iso_639_1: "es",
    french_name: "Espagnol",
    name: "Español"
  },
  {
    iso_639_1: "en",
    french_name: "Anglais",
    name: "English"
  },
  ]
  queryUpdate$: BehaviorSubject<string> = new BehaviorSubject<string>(null)
  queryGroup:FormGroup;

  filtersUpdate$:BehaviorSubject<SearchMoviesFilters> = new BehaviorSubject<SearchMoviesFilters>(null);
  filtersFormGroup:FormGroup;

  sliderValue$:Observable<string>;

  constructor(
    private readonly filmWebService:FilmWebService,
    private readonly formBuilder:FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    ) {   
      this.filtersFormGroup = this.formBuilder.group({
        year: [null],
        genre: [''],
        language: [''],
        max_duration: [''],
      })

      this.queryGroup = this.formBuilder.group({
        query:['']
      })
   }

  ngOnInit(): void {  

    this.route.queryParamMap.pipe(distinctUntilChanged(),tap((queryParamMap:any) => {
      const year=queryParamMap.get('year') ?? null
      const language=queryParamMap.get('language') ?? null
      const with_genres=queryParamMap.get('genre') ?? null
      const with_runtime=queryParamMap.get('maxDuration') ?? null;
      this.filtersUpdate$.next({year,language,with_genres,with_runtime:{gte:null,lte:with_runtime}});

      const query=queryParamMap.get('q') ?? null
      this.queryUpdate$.next(query);
    })).subscribe()    

    this.listFilm$ = this.route.queryParamMap.pipe(
      switchMap((queryParamMap:any) => {
        const year=queryParamMap.get('year') ?? null
        const language=queryParamMap.get('language') ?? null
        const with_genres=queryParamMap.get('genre') ?? null
        const with_runtime={gte:null,lte:queryParamMap.get('maxDuration')};
        const query=queryParamMap.get('q') ?? null
        if(!!query){
          return this.filmWebService.searchMovies(query,language,year).pipe(
            first(),
            switchMap( (filmList:FilmList) => {
              const results = filmList.results.filter((film:FilmResult) => {
              let boo:boolean=true;
              if(year){
                boo=boo&&film.release_date.split('-')[0]===`${year}`
              }  
              if(with_genres){
                boo = boo && !!film.genre_ids.find((genre:number)=> genre === +with_genres)
              }  
              return boo;
            })
            return of({...filmList,results,total_results:results.length});
           }))
        } 
        return this.filmWebService.getMovies({year,language,with_genres,with_runtime})
        })
      )
  
    this.filtersUpdate$.subscribe((filterUpdate:SearchMoviesFilters)=>{
      this.filtersFormGroup.get('genre').setValue(+filterUpdate.with_genres)
      this.filtersFormGroup.get('year').setValue(filterUpdate.year)
      this.filtersFormGroup.get('language').setValue(filterUpdate.language)
      this.filtersFormGroup.get('max_duration').setValue(filterUpdate.with_runtime.lte)
    })

    this.queryUpdate$.subscribe((query:string)=> {
      this.queryGroup.get('query').setValue(query);
    })

    this.sliderValue$ = 
      this.filtersFormGroup.get('max_duration').valueChanges.pipe(
        distinctUntilChanged(),
        filter((duration)=>!!duration || duration===0),
        switchMap((duration:string) => {
        const hours=Math.trunc(+duration / 60);
        const minutes=+duration-hours*60;
        let result='';
        if(hours>0){
          result=result+hours+'h';
        }
        if(minutes>0){
          result=result+minutes+'mn';
        }
        return of(result);
      }
    ))
  }
  
  stopPropagation(event){
    event.stopPropagation();
  }

  submitFilters(){
    const filters:SearchMoviesFilters={
      with_genres:this.filtersFormGroup.get('genre').value??'',
      year:this.filtersFormGroup.get('year').value??'',
      language:this.filtersFormGroup.get('language').value??'',
      with_runtime:this.filtersFormGroup.get('max_duration').value??''
    }
    this.filtersUpdate$.next(filters)

    this.router.navigate([`${this.router.url.split('?')[0]}`], {
      queryParamsHandling: 'merge',
      queryParams: {
        language: !!filters.language ? filters.language : null,
        genre: !!filters.with_genres ? filters.with_genres : null,
        year: !!filters.year ? filters.year : null,
        maxDuration: !!filters.with_runtime ? filters.with_runtime : null,
      },
    });
  }

  submitQuery(){
    const query = this.queryGroup.get('query').value
    this.queryUpdate$.next(query)

    this.router.navigate([`${this.router.url.split('?')[0]}`], {
      queryParamsHandling: 'merge',
      queryParams: {
        q: !!query ? query : null,
      },
    });
  }
}