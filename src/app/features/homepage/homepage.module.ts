import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { RouterModule } from "@angular/router";
import { FilmComponent } from "./film/film.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { MatMenuModule } from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSliderModule} from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
    imports: [
      CommonModule,
       RouterModule.forChild([
        {
          path: '',
          component: HomepageComponent,
        }
      ]),
      ReactiveFormsModule,
      MatFormFieldModule,
      MatInputModule,
      MatMenuModule,
      MatIconModule,
      MatSliderModule,
      MatSelectModule,
      MatButtonModule
    ],
    providers: [],
    declarations: [
      HomepageComponent,
      FilmComponent
    ],
  })
  export class HomePageModule {}
  