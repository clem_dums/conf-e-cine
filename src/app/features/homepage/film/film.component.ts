import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FilmResult } from 'src/app/models/filmTmdb';
import { Router, RouterModule } from '@angular/router';
import { FeaturesRoutingEnum } from '../../features-routing-enum';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilmComponent implements OnInit {

  @Input() filmResult: FilmResult;
  
  constructor(private router:Router) { }

  goToFilm() {
    this.router.navigate([FeaturesRoutingEnum.Film], { queryParams: { id: this.filmResult.id } });
  }

  ngOnInit(): void {
  }

}
