import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturesRoutingEnum } from './features-routing-enum';
import { MustBeConnectedGuard } from './guards/must-be-connected.guard';
import { NotConnectedGuard } from './guards/not-connected-guard';

const routes: Routes = [];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: FeaturesRoutingEnum.SignIn,
        canActivate: [NotConnectedGuard],
        loadChildren: () => import('./sign-in/sign-in.module').then((m) => m.SignInModule),
      },      
      {
        path: FeaturesRoutingEnum.Member,
        canActivate: [MustBeConnectedGuard],
        loadChildren: () => import('./member/member.module').then((m) => m.MembersModule),
      },
      {
        path: FeaturesRoutingEnum.HomePage,
        loadChildren: () => import('./homepage/homepage.module').then((m) => m.HomePageModule),
      },
      {
        path: FeaturesRoutingEnum.List,
        canActivate: [MustBeConnectedGuard],
        loadChildren: () => import('./movie-list/list.module').then((m) => m.MovieListModule),
      },
	  {
        path: FeaturesRoutingEnum.Film,
        loadChildren: () => import('./film-details/film-details.module').then((m) => m.FilmDetailsModule),
      },
    ]),

    ],

  providers: [
    MustBeConnectedGuard,
    NotConnectedGuard
  ],

})
export class FeaturesModule { }
