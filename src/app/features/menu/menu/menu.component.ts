import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { FeaturesRoutingEnum } from '../../features-routing-enum';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnDestroy{

  get isConnected$(): Observable<boolean>{
    return this.signInServiceService.isConnected$;
  }

  constructor(private readonly signInServiceService: SignInServiceService, private router:Router) { }

  goToHome() {
    this.router.navigate([FeaturesRoutingEnum.HomePage]);
  }

  goToList() {
    this.router.navigate([FeaturesRoutingEnum.List]);
  }

  goToMember() {
    this.router.navigate([FeaturesRoutingEnum.Member]);
  }
  
  goToLogin() {
    this.router.navigate([FeaturesRoutingEnum.SignIn]);
  }

  ngOnDestroy(): void{
    this.signInServiceService.onLeavePage();
  }
}
