import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { SignInServiceService } from '../../services/sign-in/sign-in-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit, OnDestroy {

  get user$(): Observable<User> {
    return this.signInServiceService.user$;
  }

  profileFormGroup: FormGroup;

  inputItems: InputItem[] = [
    {
      label: 'Prénom',
      type: 'text',
      formControlName: 'firstName',
      class: 'firstName'
    },
    {
      label: 'Nom',
      type: 'text',
      formControlName: 'lastName',
      class: 'lastName'
    },
    {
      label: 'Email',
      type: 'text',
      formControlName: 'email',
      class: 'email'
    },
    {
      label: 'Téléphone',
      type: 'text',
      formControlName: 'phoneNumber',
      class: 'phoneNumber'
    },
  ];

  constructor(
    private readonly signInServiceService: SignInServiceService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly location: Location
    ) { }

  ngOnInit(): void {
    this.user$.pipe(first()).subscribe((user: User)=>{
      this.profileFormGroup = this.formBuilder.group({
        firstName: [user.firstName || '', Validators.required],
        lastName: [user.lastName || '', Validators.required],
        phoneNumber: [user.phoneNumber || ''],
        email: [user.email || ''],
      });
    });
  }

  goBack(): void {
    this.location.back();
  }

  trackByInputItem(_, inputItem: InputItem): string {
    return inputItem.formControlName;
  }

  signOut(): void{
    this.signInServiceService.signOutUser();
    setTimeout(() => {
      this.router.navigate(['/sign-in']);
    }, 1000);
  }

  ngOnDestroy(): void{
    this.signInServiceService.onLeavePage();
  }
}


export interface InputItem {
  label: string;
  type: string;
  formControlName: string;
  required?: boolean;
  class?: string;
}
