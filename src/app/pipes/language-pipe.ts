import { CommonModule } from '@angular/common';
import { NgModule, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'language',
})
export class LanguagePipe implements PipeTransform {
    languages:{iso_639_1:string, french_name:string, name:string, }[] = [
        {
          iso_639_1: "de",
          french_name: "Allemand",
          name: "Deutsch"
        },
        
        {
          iso_639_1: "fr",
          french_name: "Français",
          "name": "Français"
        },
        {
          iso_639_1: "it",
          french_name: "Italien",
          name: "Italiano"
        },
        {
          iso_639_1: "ru",
          french_name: "Russe",
          name: "Pусский"
        },
        {
          iso_639_1: "es",
          french_name: "Espagnol",
          name: "Español"
        },
        {
          iso_639_1: "en",
          french_name: "Anglais",
          name: "English"
        },
      ]

  transform(event: string): string {
    const returnValue= this.languages.find((language:{iso_639_1:string, french_name:string, name:string, })=>
      language.iso_639_1===event
    )
    return returnValue?returnValue.french_name : event;
  }
}

@NgModule({
  imports: [CommonModule],
  exports: [LanguagePipe],
  declarations: [LanguagePipe],
})
export class LanguagePipeModule {}
